package com.example.grolemetronome;

import static java.lang.Math.max;

import androidx.appcompat.app.AppCompatActivity;

import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    SoundPool soundPool;
    int soundBeep, soundClick, soundWood,soundDing;
    Random rn = new Random();
    ArrayList<Integer> patternFileIDs = new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initial();
    }


    // int number = rn.nextInt(11);
    private void loadPatternNumbers(){
        for (int i=1; i<=10; i++){

            String filename = "en_num_" + numberStr;
            System.out.println("FILENAME: " + filename);
            return getResources().getIdentifier(filename,
                    "raw", getPackageName());


        }

    }


    private TimerTask getPlaySoundTask(int soundID){
        return new TimerTask() {
            public void run() {
                soundPool.play(soundID, 1, 1, 0, 0, 1);
            }
        };
    }

    private TimerTask playPatternNumberTask = new TimerTask() {
        @Override
        public void run() {
            soundPool.play(getRandomPattern(), 1, 1, 0, 0, 1);
        }
    };

    private int getRandomPattern(){
        int number = rn.nextInt(11);
        String numberStr = Integer.toString(number);
        if (number < 10){
            numberStr = "0" + numberStr;
        }

        String filename = "en_num_" + numberStr;
        System.out.println("FILENAME: " + filename);
        return getResources().getIdentifier(filename,
                "raw", getPackageName());
    }


    private void initial(){
        System.out.println("Start");


        AudioAttributes audioAttributes = new AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_MEDIA)
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC).build();
        soundPool = new SoundPool.Builder().setMaxStreams(6).setAudioAttributes(audioAttributes).build();



        soundBeep = soundPool.load(this, R.raw.beep, 1);
        soundClick = soundPool.load(this, R.raw.click, 1);
        soundDing = soundPool.load(this, R.raw.ding, 1);
        soundWood = soundPool.load(this, R.raw.wood, 1);

        Timer timerFour = new Timer();
        Timer timerEight = new Timer();
        Timer timerNumber = new Timer();


        int bpm = 120;
        long period = (long) 60000/bpm;
        long patternDelay = (long) (period*3.0/4.0);
        // timerFour.schedule(getPlaySoundTask(soundClick),0, period);
        // timerEight.schedule(getPlaySoundTask(soundWood),(long)period/2, period);
        // timerNumber.schedule(playPatternNumberTask,patternDelay, period*4);
        int patternId = getRandomPattern();
        soundPool.play(patternId, 1, 1, 0, 0, 1);


    }

}